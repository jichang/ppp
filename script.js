import http from 'k6/http';
import { sleep } from 'k6';

var payload1 = JSON.stringify({
	"metadata": {
		"packageName": "string",
		"windowsAccount": "string",
		"priority": "string"
	},
	"spec": {
		"title": "string",
		"body": "string",
		"userDefined": {
			"additionalProp1": "string",
			"additionalProp2": "string",
			"additionalProp3": "string"
		},
		"systemConfig": {
			"additionalProp1": "string",
			"additionalProp2": "string",
			"additionalProp3": "string"
		}
	}
});

var payload2 = JSON.stringify({
	"serialNumber": "string",
	"packageName": "string",
	"token": "string",
	"account": "string"
});


const params = {
	headers: {
		'Content-Type': 'application/json',
	},
};

export const options = {
	vus: 50, //代表模擬用戶數量
	duration: '10s', //代表執行時間
	rps: 5000
	//discardResponseBodies: true,
	//scenarios: {
	//	contacts: {
	//		executor: 'constant-arrival-rate',
	//		rate: 600, // 2 RPS, since timeUnit is the default 1s
	//		duration: '10s',
	//		preAllocatedVUs: 50000,
	//		maxVUs: 100000,
	//	},
	// }
};

export default function() {
	//http.get('http://localhost:8080/notification/test')
	http.post('http://localhost:8080/notification/send', payload1, params); //測試目標網址
	//http.post('http://localhost:8080/notification/register', payload2, params); //測試目標網址
}