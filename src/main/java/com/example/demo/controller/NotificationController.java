package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.schema.RegisterDeviceTokenRequest;
import com.example.demo.schema.SendPushMessageRequest;
import com.example.demo.schema.SendPushMessageResponse1;
import com.example.demo.schema.SendPushMessageResponse2;
import com.example.demo.service.NotificationService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/notification")
@Tag(name = "Notification", description = "Push Notification")
public class NotificationController {

	Logger logger = LoggerFactory.getLogger(NotificationController.class);

	@Autowired
	NotificationService service;

	@Operation(summary = "[Sync] Register th device token", description = "")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "successful operation") })
	@GetMapping(value = "test")
	public ResponseEntity<?> test() {
//		service.register(request);
		return ResponseEntity.ok().build();
	}

	@Operation(summary = "[Sync] Register th device token", description = "")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "successful operation") })
	@PostMapping(value = "register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> register(@RequestBody RegisterDeviceTokenRequest request) {
		logger.info("register = " + request.getPackageName());
//		service.register(request);
		return ResponseEntity.ok().build();
	}

	@Operation(summary = "[Async] Send message", description = "Adds a plant to the list of plants")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "successfully added a plnt", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = SendPushMessageResponse1.class)) }),
			@ApiResponse(responseCode = "202", description = "successfully added a plnt", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = SendPushMessageResponse2.class)) }),
			@ApiResponse(responseCode = "400", description = "duplicate plant") })
	@PostMapping(value = "send")
	public ResponseEntity<?> send(@RequestHeader(required = false) String callback,
			@RequestHeader(defaultValue = "false", required = false) boolean async,

			@RequestBody SendPushMessageRequest request) {
		logger.info("send = " + request);
		return ResponseEntity.ok(service.sendMessage(async, request));

	}

//	@Autowired	
//	NotificationService service;
//
//	@Override
//	public ResponseEntity<Void> register(RegisterDeviceTokenRequest request) {
//		logger.info("register " + request)	;
//		service.register(request);
//		return new ResponseEntity<>(HttpStatus.OK);
//	}
//
//	@Override
//	public ResponseEntity<Object> send(SendPushMessageRequest request) {
//		logger.info("send " + request);
////		if (async)
////			return	 new ResponseEntity<>(service.sendAsyncMessage(request), HttpStatus.ACCEPTED);
//		return ResponseEntity.ok(service.sendMessage(request));
//	}
}
