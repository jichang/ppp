package com.example.demo.schema;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SendPushMessageRequest {

	Metadata metadata;
	PushMessageSpec spec;

}

@Data
@AllArgsConstructor
class Metadata {
	String packageName;
	String windowsAccount;
	String priority;
}
