package com.example.demo.schema;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PushMessageSpec {

	String title;
	String body;

	Map<String, String> userDefined;
	Map<String, String> systemConfig;

}
