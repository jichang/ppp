package com.example.demo.schema;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Model retrieved from library system")
public class RegisterDeviceTokenRequest {

	@ApiModelProperty(notes = "Title of the book", example = "Inside of this hell")
	String serialNumber;
	@ApiModelProperty(notes = "Title of the book", example = "Inside of this hell")
	String packageName;

	@ApiModelProperty(notes = "Title of the book", example = "Inside of this hell")
	String token;
	@ApiModelProperty(notes = "Title of the book", example = "Inside of this hell")
	String account;

}
