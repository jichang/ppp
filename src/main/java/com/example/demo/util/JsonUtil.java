package com.example.demo.util;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

public class JsonUtil {

	protected static final Logger LOG = LoggerFactory.getLogger(JsonUtil.class);

	protected static final ObjectMapper MAPPER;

	static {
		MAPPER = new ObjectMapper();
		MAPPER.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		MAPPER.registerModule(new ParameterNamesModule());
		MAPPER.registerModule(new Jdk8Module());
		MAPPER.registerModule(new JavaTimeModule());
	}

	public static final ObjectMapper getObjectMapper() {
		return MAPPER;
	}

	public static final JsonNode toJsonNode(String json) throws JsonMappingException, JsonProcessingException {
		return MAPPER.readTree(json);
	}

	public static final String getField(String json, String field)
			throws JsonMappingException, JsonProcessingException {
		JsonNode node = toJsonNode(json);
		node = node.get(field);
		if (node != null) {
			return node.asText();
		}
		return null;
	}

	public static final <T> T fromJson(Reader r, Class<T> clazz) throws IOException {
		return MAPPER.readValue(r, clazz);
	}

	public static final <T> T fromJson(String json, Class<T> clazz) throws IOException {
		return fromJson(new StringReader(json), clazz);
	}

	public static final void toJson(Writer w, Object value) throws IOException {
		MAPPER.writeValue(w, value);
	}

	public static <T> List<T> toList(String pJson, Class<T> clazz) {
		try {
			return MAPPER.readValue(pJson, MAPPER.getTypeFactory().constructCollectionType(List.class, clazz));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return Collections.emptyList();
	}

	public static List<Map<String, Object>> toListMap(String pJson) {
		try {
			return MAPPER.readValue(pJson, MAPPER.getTypeFactory().constructCollectionType(List.class, Map.class));
		} catch (Exception e) {
			LOG.error(pJson);
			LOG.error(e.getMessage(), e);
		}
		return Collections.emptyList();
	}

	public static <T> List<T> toObject(String pJson, TypeReference<List<T>> pTypeReference) {
		try {
			return MAPPER.readValue(pJson, pTypeReference);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return Collections.emptyList();
	}

	public static final <T> T toObject(String pJson, Class<T> clazz) {
		try {
			if (CommonUtil.isNotEmptyOrSpace(pJson)) {
				return toObjectException(pJson, clazz);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

	public static final <T> T toObjectException(String pJson, Class<T> clazz) throws IOException {
		return MAPPER.readValue(pJson, clazz);
	}

	public static final <T> T toObject(Reader r, Class<T> clazz) {
		try {
			return MAPPER.readValue(r, clazz);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

	public static final String toJson(Object value) {
		try {
			if (value != null) {
				return MAPPER.writeValueAsString(value);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

	public static final String toPrettyJson(Object value) {
		try {
			return MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(value);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> toMap(String pJson) {
		return Optional.ofNullable(toObject(pJson, Map.class)).orElse(Collections.emptyMap());
	}

	@SuppressWarnings("unchecked")
	public static Map<String, String> toStringMap(String pJson) {
		return Optional.ofNullable(toObject(pJson, Map.class)).orElse(Collections.emptyMap());
	}
}