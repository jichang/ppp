package com.example.demo.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RootException extends RuntimeException {

	private static final long serialVersionUID = 2340831176382112562L;

	private int errorCode;
	private String message;

	public RootException(String pDescMessage) {
		super(pDescMessage);
		setMessage(pDescMessage);
	}

}