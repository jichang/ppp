package com.example.demo.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.common.collect.Lists;

public class CommonUtil {

	private CommonUtil() {
	}

	public static boolean contains(Collection<?> pContainer, Object pValue) {
		return pContainer.contains(pValue);
	}

	public static boolean contains(final Object[] pContainer, final Object pValue) {
		if (CommonUtil.isNotEmptyOrSpace(pContainer) && pValue != null) {
			for (Object o : pContainer) {
				if (pValue.equals(o)) {
					return true;
				}
			}
		}
		return false;
	}

	public static <T> T getEmptyVauleByDefault(T value, T defValue) {
		return isNotEmptyOrSpace(value) ? value : defValue;
	}

	@SafeVarargs
	public static <T> List<T> getListContent(final T... pObject) {
		if (isNotEmptyOrSpace(pObject)) {
			return Lists.newArrayList(pObject);
		}
		return Collections.emptyList();
	}

	@SafeVarargs
	public static <T> List<T> getListContentByList(List<T>... elements) {
		List<T> tOut = new ArrayList<>();
		if (CommonUtil.isNotEmptyOrSpace(elements)) {
			for (List<T> l : elements) {
				tOut.addAll(l);
			}
		}
		return tOut;
	}

	public static <T extends Comparable<? super T>> List<T> newSortedList(List<T> elements) {
		return newSortedList(elements, null);
	}

	public static <T extends Comparable<? super T>> List<T> newSortedList(List<T> elements, Comparator<T> comparator) {
		List<T> ns = new ArrayList<>(elements.size());
		ns.addAll(elements);
		ns.sort(comparator);

		return ns;
	}

	public static <T> T getNullVauleByDefault(T value, T defValue) {
		return Optional.ofNullable(value).orElse(defValue);
	}

	public static String getEmptyorNullVauleByDefault(String value, String defValue) {
		return CommonUtil.isEmptyOrSpace(value) ? defValue : value;
	}

	public static boolean isArray(final Object pObj) {
		if (pObj != null) {
			return pObj.getClass().isArray();
		}
		return false;
	}

	public static boolean isEmptyOrSpace(final CharSequence pValue) {
		return (pValue == null || pValue.length() <= 0);
	}

	public static boolean isEmptyOrSpace(final Collection<?> pValue) {
		return (pValue == null || pValue.isEmpty());
	}

	public static boolean isEmptyOrSpace(final Integer pValue) {
		return (pValue == null || pValue == 0);
	}

	public static boolean isEmptyOrSpace(final Map<?, ?> pValue) {
		return (pValue == null || pValue.size() <= 0);
	}

	public static boolean isEmptyOrSpace(final Object pValue) {
		if (pValue == null) {
			return true;
		}
		if (pValue instanceof Collection<?>) {
			return isEmptyOrSpace((Collection<?>) pValue);
		}
		if (pValue instanceof CharSequence) {
			return isEmptyOrSpace((CharSequence) pValue);
		}
		if (pValue instanceof Object[]) {
			return isEmptyOrSpaceArray((Object[]) pValue);
		}
		return false;
	}

	public static <T> boolean isEmptyOrSpaceArray(final T[] pValue) {
		return (pValue == null || pValue.length <= 0);
	}

	public static boolean isEmptyOrSpaceArray(final int[] pValue) {
		return (pValue == null || pValue.length <= 0);
	}

	public static boolean isEmptyOrSpace(final String pValue) {
		return (pValue == null || pValue.length() <= 0);
	}

	public static boolean isNotEmptyOrSpace(final CharSequence pValue) {
		return !isEmptyOrSpace(pValue);
	}

	public static boolean isNotEmptyOrSpace(final Collection<?> pValue) {
		return !isEmptyOrSpace(pValue);
	}

	public static boolean isNotEmptyOrSpace(final Integer pValue) {
		return !isEmptyOrSpace(pValue);
	}

	public static boolean isNotEmptyOrSpace(final Map<?, ?> pValue) {
		return !isEmptyOrSpace(pValue);
	}

	public static boolean isNotEmptyOrSpace(final Object pValue) {
		return !isEmptyOrSpace(pValue);
	}

	public static <T> boolean isNotEmptyOrSpaceArray(final T[] pValue) {
		return !isEmptyOrSpaceArray(pValue);
	}

	public static boolean isNotEmptyOrSpaceArray(final int[] pValue) {
		return !isEmptyOrSpaceArray(pValue);
	}

	public static boolean isNotEmptyOrSpace(final String pValue) {
		return !isEmptyOrSpace(pValue);
	}

	public static <T> T trimDataEscape(T t) {
		return t;
	}

	public static void doNothing(Object... e) {
	}

}