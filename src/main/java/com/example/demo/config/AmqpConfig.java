package com.example.demo.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AmqpConfig {

	public static final String EXCHANGE = "tws.pnm.message";

	public static final String QUEUE_NORMAL_READY = "normal.ready";
	public static final String QUEUE_NORMAL_WAIT = "normal.wait";

	public static final String QUEUE_HIGH_REDAY = "high.ready";
	public static final String QUEUE_HIGH_WAIT = "high.wait";

	@Bean
	DirectExchange exchange() {
		return new DirectExchange(EXCHANGE);
	}

	@Bean
	Queue normal_ready_queue() {
		return new Queue(QUEUE_NORMAL_READY);
	}

	@Bean
	Queue normal_wait_queue() {
		return new Queue(QUEUE_NORMAL_WAIT);
	}

	@Bean
	Binding normal_ready_binding(Queue normal_ready_queue, DirectExchange exchange) {
		return BindingBuilder.bind(normal_ready_queue).to(exchange).with("normal.ready");
	}

	@Bean
	Binding normal_wait_binding(Queue normal_wait_queue, DirectExchange exchange) {
		return BindingBuilder.bind(normal_wait_queue).to(exchange).with("normal.wait");
	}

	@Bean
	Queue high_ready_queue() {
		return new Queue(QUEUE_HIGH_REDAY);
	}

	@Bean
	Queue high_wait_queue() {
		return new Queue(QUEUE_HIGH_WAIT);
	}

	@Bean
	Binding high_ready_binding(Queue high_ready_queue, DirectExchange exchange) {
		return BindingBuilder.bind(high_ready_queue).to(exchange).with("high.ready");
	}

	@Bean
	Binding high_wait_binding(Queue high_wait_queue, DirectExchange exchange) {
		return BindingBuilder.bind(high_wait_queue).to(exchange).with("high.wait");
	}

//	@Bean
//	SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
//			MessageListenerAdapter listenerAdapter) {
//		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
//		container.setConnectionFactory(connectionFactory);
//		container.setQueueNames(queueName);
//		container.setMessageListener(listenerAdapter);
//		return container;
//	}
//
//	@Bean
//	MessageListenerAdapter listenerAdapter(Receiver receiver) {
//		return new MessageListenerAdapter(receiver, "receiveMessage");
//	}

}
