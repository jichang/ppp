package com.example.demo.service;

import com.example.demo.schema.RegisterDeviceTokenRequest;
import com.example.demo.schema.SendPushMessageRequest;
import com.example.demo.schema.SendPushMessageResponse2;

public interface NotificationService {

	/* bad design, service used controller entity */
	boolean register(RegisterDeviceTokenRequest identity);

	SendPushMessageResponse2 sendMessage(boolean async, SendPushMessageRequest message);

//	Result sendBulkMessage(BulkMessage message);

}
