package com.example.demo.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Device;
import com.example.demo.repository.DeviceRepository;
import com.example.demo.schema.RegisterDeviceTokenRequest;
import com.example.demo.schema.SendPushMessageRequest;
import com.example.demo.schema.SendPushMessageResponse2;
import com.example.demo.service.ConvertService;
import com.example.demo.service.MessageService;
import com.example.demo.service.NotificationService;

@Service
public class NotificationServiceImpl implements NotificationService {

	Logger logger = LoggerFactory.getLogger(NotificationServiceImpl.class);

	@Autowired
	DeviceRepository repository;

	@Autowired
	ConvertService convertService;

	@Autowired
	MessageService messageService;

	@Override
	public boolean register(RegisterDeviceTokenRequest identity) {
		Device device = repository.queryByPackageNameAndSerialNumber(identity.getPackageName(),
				identity.getSerialNumber());
		device = (device == null) ? convertService.convert(identity) : convertService.convert(identity, device);
		logger.info("device = " + device);
		repository.save(device);
		return true;
	}

	@Override
	public SendPushMessageResponse2 sendMessage(boolean async, SendPushMessageRequest message) {
		messageService.sendMessage(message);
		return null;
	}

}
