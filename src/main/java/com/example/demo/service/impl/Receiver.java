package com.example.demo.service.impl;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.example.demo.controller.NotificationController;

@Component
public class Receiver {

	Logger logger = LoggerFactory.getLogger(NotificationController.class);

	private CountDownLatch latch = new CountDownLatch(1);

	public void receiveMessage(String message) {
//		logger.info("Received <" + message + ">");
//		latch.countDown();
	}

	public CountDownLatch getLatch() {
		return latch;
	}

}