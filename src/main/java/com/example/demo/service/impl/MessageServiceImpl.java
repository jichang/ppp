package com.example.demo.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.config.AmqpConfig;
import com.example.demo.schema.SendPushMessageRequest;
import com.example.demo.service.MessageService;
import com.example.demo.util.JsonUtil;

@Service
public class MessageServiceImpl implements MessageService {

	private static final Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);

	@Autowired
	protected RabbitTemplate template;

	@Override
	public boolean sendMessage(SendPushMessageRequest request) {
		logger.info("Send to mq");
//		template.convertAndSend(null, request);
		template.convertAndSend(AmqpConfig.QUEUE_NORMAL_READY, JsonUtil.toJson(request));
//		template.convertAndSend(AmqpConfig.EXCHANGE, AmqpConfig.QUEUE_NORMAL_READY, JsonUtil.toJson(request));

		return false;
	}

//	@RabbitListener(queues = "#{normal_ready_queue.name}")
//	public void receive_normal(List<String> messages) throws InterruptedException {
//
//		logger.info("" + messages);
//	}
//
//	@RabbitListener(queues = "#{high_ready_queue.name}")
//	public void receive_high(String message) throws InterruptedException {
//
//		logger.info("receive_high" + message);
//
//	}

}
