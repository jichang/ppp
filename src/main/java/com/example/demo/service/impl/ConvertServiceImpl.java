package com.example.demo.service.impl;

import org.springframework.stereotype.Service;

import com.example.demo.entity.Device;
import com.example.demo.schema.RegisterDeviceTokenRequest;
import com.example.demo.service.ConvertService;

@Service
public class ConvertServiceImpl implements ConvertService {

	@Override
	public Device convert(RegisterDeviceTokenRequest request, Device device) {
		device.setAccount(request.getAccount());
		device.setDeviceToken(request.getToken());
		device.setPackageName(request.getPackageName());
		device.setSerialNumber(request.getSerialNumber());
		return device;
	}

}
