package com.example.demo.service;

import com.example.demo.entity.Device;
import com.example.demo.schema.RegisterDeviceTokenRequest;

public interface ConvertService {

	default Device convert(RegisterDeviceTokenRequest request) {
		return convert(request, new Device());
	}

	Device convert(RegisterDeviceTokenRequest request, Device device);

}
