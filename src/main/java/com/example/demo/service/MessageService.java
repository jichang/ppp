package com.example.demo.service;

import com.example.demo.schema.SendPushMessageRequest;

public interface MessageService {

	public boolean sendMessage(SendPushMessageRequest request);

}
